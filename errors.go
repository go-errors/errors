package errors

import (
	"golang.org/x/xerrors"
)

func New(text string) error {
	return xerrors.New(text)
}

func Is(err, target error) bool {
	return xerrors.Is(err, target)
}

func As(err, target error) bool {
	if err == nil || target == nil {
		return false
	}
	return xerrors.As(err, target)
}

func Unwrap(err error) error {
	if err == nil {
		return nil
	}
	return xerrors.Unwrap(err)
}

func Opaque(err error) error {
	if err == nil {
		return nil
	}
	return xerrors.Opaque(err)
}

func Errorf(format string, a ...interface{}) error {
	return xerrors.Errorf(format, a...)
}

func Wrap(err error) error {
	if err == nil {
		return nil
	}
	return xerrors.Errorf("%w", err)
}
